" ===========================================================================
" Plugin manager
let g:python_host_prog='/usr/bin/python'

call plug#begin('~/.local/share/nvim/plugged')

" Aspect
Plug 'itchyny/lightline.vim'
Plug 'nanotech/jellybeans.vim'

" Linting/Completion
Plug 'autozimu/LanguageClient-neovim', {
   \ 'branch': 'next',
   \ 'do': 'bash install.sh',
   \ }
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-dispatch'

" (Optional) Multi-entry selection UI.
Plug '/usr/local/opt/fzf'
Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'

" Git support
Plug 'tpope/vim-fugitive'

call plug#end()
" ===========================================================================

" ===========================================================================
" Aspect
color jellybeans
set number
set hidden
set signcolumn=yes
set inccommand=nosplit

set completeopt=longest,menuone,preview
set hidden
" ===========================================================================

" ===========================================================================
" Linting / Completion
let g:LanguageClient_serverCommands = {
     \ 'python': ['pyls'],
     \ 'c' : ['clangd'],
     \ 'cpp' : ['clangd'],
     \ 'cs' : ['omnisharp', '-stdio']
     \ }

let g:LanguageClient_rootMarkers = {
    \ 'cs' : ['*.csproj']
    \ }

let g:LanguageClient_loggingFile = expand('~/.local/share/nvim/LanguageClient.log')
let g:LanguageClient_serverStderr = expand('~/.local/share/nvim/LanguageServer.log')
let g:LanguageClient_loadSettings = 0
" let g:LanguageClient_useVirtualText=0
let g:deoplete#enable_at_startup = 1 " Use deoplete.

imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <expr><TAB>
 \ pumvisible() ? "\<C-n>" :
 \ neosnippet#expandable_or_jumpable() ?
 \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

" tabs
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" ===========================================================================

" ===========================================================================
" leader and commands
let mapleader = "\<Space>"
nnoremap <silent> yb :buffers<CR>
nnoremap <silent> yq :copen<CR>
nnoremap <silent> yQ :cclose<CR>
nnoremap <silent> <leader>ld :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <leader>lr :call LanguageClient#textDocument_rename()<CR>
nnoremap <silent> <leader>lf :call LanguageClient#textDocument_formatting()<CR>
nnoremap <silent> <leader>lt :call LanguageClient#textDocument_typeDefinition()<CR>
nnoremap <silent> <leader>lx :call LanguageClient#textDocument_references()<CR>
nnoremap <silent> <leader>ls :call LanguageClient_textDocument_documentSymbol()<CR>
nnoremap <silent> <leader>s :ToggleWorkspace<CR>

nnoremap <silent> <Leader>e :FZF<CR>

command Go G | only

" highlight trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+\%#\@<!$/

" ===========================================================================

" ===========================================================================
" Lightline config
let g:lightline = {
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
    \ },
    \ 'component_function': {
    \   'gitbranch': 'fugitive#head'
    \ },
    \ 'colorscheme': 'jellybeans',
    \ }
