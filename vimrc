" "" ensimag vim config file version 1.0.2
" "" this file is intended for vim 8. at ensimag everyting is pre-configured.
" "" before using it on your machine however you will need to:
" "" - install plug with :
" ""      curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" "" (see https://github.com/junegunn/vim-plug)
" "" - install the languageserver server for each language you indend to use :
" ""    * pyls for python (see https://github.com/palantir/python-language-server)
" ""    * rls for rust (see https://github.com/rust-lang-nursery/rls)
" ""    * clangd for c
" "" - you need to install jedi for python auto-completion
" "" - install some font with powerline symbols for eye candy and icons
" "" (see https://github.com/powerline/fonts)
" "" - change plugin directory to ~/.vim/plugged
" "" (uncomment line 23 and comment line 22)
" 
" "" after that copy this file as your ~/.vimrc and execute :PlugInstall
" 
" set nocompatible
" filetype off
" 
" " call plug#begin('/etc/vim/plugged') " at ensimag
" call plug#begin('~/.vim/plugged') " on your own machine
" 
" Plug 'tpope/vim-sensible' " sane defaults
" 
" " eye candy
" Plug 'vim-airline/vim-airline' " status bar (needs special fonts)
" Plug 'vim-airline/vim-airline-themes'
" Plug 'morhetz/gruvbox' " very nice and soft color theme
" Plug 'ryanoasis/vim-devicons' " various symbols (linux, rust, python, ...)
" 
" " essential plugins
" " see for example https://github.com/autozimu/LanguageClient-neovim/issues/35#issuecomment-288731665
" Plug 'maralla/completor.vim' " auto-complete
" Plug 'w0rp/ale' " linters
" 
" " rust
" Plug 'rust-lang/rust.vim' " syntax highlighting
" Plug 'mattn/webapi-vim' " used for rust playpen
" 
" 
" " not essential
" Plug 'vim-scripts/winmanager' " window manager
" Plug 'scrooloose/nerdtree' " browse files tree
" Plug 'vim-scripts/taglist.vim' " taglist
" Plug 'junegunn/fzf' " fuzzy files finding
" 
" " snippets allow to easily 'fill' common patterns
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'
" 
" " Plugin pour voir du latex en live
" " Plug 'lervag/vimtex' " Un peu lent
" Plug 'xuhdev/vim-latex-live-preview' " Juste le preview
" 
" " Plug 'Mambu38/tele-vim'
" 
" call plug#end()
" 
" filetype plugin indent on
" 
" " configure maralla/completor to use tab
" " other configurations are possible (see website)
" inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
" inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" inoremap <expr> <cr> pumvisible() ? "\<C-y>\<cr>" : "\<cr>"
" 
" " ultisnips default bindings compete with completor's tab
" " so we need to remap them
" let g:UltiSnipsExpandTrigger="<c-t>"
" let g:UltiSnipsJumpForwardTrigger="<c-b>"
" let g:UltiSnipsJumpBackwardTrigger="<c-z>"
" 
" " airline :
" " for terminology you will need either to export TERM='xterm-256color'
" " or run it with '-2' option
" let g:airline_powerline_fonts = 1
" set laststatus=2
" au VimEnter * exec 'AirlineTheme hybrid'
" 
" set encoding=UTF-8
" 
" syntax on
" 
" colo gruvbox
" set background=dark
" set number
" 
" let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree
" 
" " replace tabs
" set tabstop=4
" set shiftwidth=4
" set softtabstop=4
" set expandtab
" 
" " highlight trailing whitespace
" highlight ExtraWhitespace ctermbg=red guibg=red
" match ExtraWhitespace /\s\+\%#\@<!$/
" 
" " some more rust
" let g:rustfmt_autosave = 1
" let g:rust_conceal = 1
" set hidden
" au BufEnter,BufNewFile,BufRead *.rs syntax match rustEquality "==\ze[^>]" conceal cchar=≟
" au BufEnter,BufNewFile,BufRead *.rs syntax match rustInequality "!=\ze[^>]" conceal cchar=≠
" 
" " vim tex params
" let g:livepreview_previewer = "open"
" let g:livepreview_cursorhold_recompile = 0
" 
" " ale params
" let g:ale_echo_msg_format = '[%linter%] (%severity%) %s'
" let g:ale_lint_on_insert_leave=1
" nmap <silent> <C-k> <Plug>(ale_previous_wrap)
" nmap <silent> <C-j> <Plug>(ale_next_wrap)
" 
" " invalid spaces
" set list listchars=nbsp:!
" 
" " hilight search
" set hlsearch
" nnoremap <F3> :set hlsearch!<CR>
" 
" " buffers managements
" nnoremap <C-h> :bp<CR>
" nnoremap <C-l> :bn<CR>
" nnoremap <C-q> :Bclose
" nnoremap <S-m> :make<CR>
" let g:airline#extensions#tabline#buffer_nr_show = 1
" let g:airline#extensions#tabline#enabled = 1
" 
" " spell config
" set  spelllang =fr,en
" set  spell
" set  spellsuggest =5
" 
" " tags configure
" nnoremap <Leader>t <C-]>
" let Tlist_GainFocus_On_ToggleOpen = 1
" 
" let g:NERDTree_title="[NERDTree]"
" function! NERDTree_Start()
"     exec "NERDTreeToggle"
" endfunction
" 
" function! NERDTree_IsValid()
"     return 1
" endfunction
" 
" let g:AutoOpenWinManager=1
" let g:persistentBehaviour=0
" let g:winManagerWidth=40
" 
" let g:winManagerWindowLayout = 'NERDTree|TagList,BufExplorer'
" 
" let g:airline_left_sep=''
" let g:airline_right_sep=''
" let g:airline_left_alt_sep = '|'
" let g:airline_right_alt_sep = '|'
" 
" au VimEnter * exec 'WMToggle'
" au VimEnter * quit
" au BufWrite * exec 'TlistUpdate'

source ~/.config/nvim/init.vim
