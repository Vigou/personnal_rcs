# Antigen
DISABLE_AUTO_UPDATE=true
ZSH="${HOME}/Library/Caches/antibody/https-COLON--SLASH--SLASH-github.com-SLASH-robbyrussell-SLASH-oh-my-zsh"

ZSH_THEME=flazz

source <(antibody init)
antibody bundle robbyrussell/oh-my-zsh
antibody bundle zsh-users/zsh-completions
antibody bundle zsh-users/zsh-syntax-highlighting
antibody bundle zsh-users/zsh-autosuggestions
# antibody theme amuse

# source <(antibody init)
# antibody bundle < ~/.zsh_plugins.txt

# Prefer vi shortcuts
bindkey -v
DEFAULT_VI_MODE=viins
KEYTIMEOUT=1

PYVEX=`python3 -c 'import pyvex; print(pyvex.__path__[0])'`
UNICORN=`python3 -c 'import unicorn; print(unicorn.__path__[0])'`
ANGR=`python3 -c 'import angr; print(angr.__path__[0])'`

install_name_tool -change libunicorn.1.dylib "$UNICORN"/lib/libunicorn.dylib "$ANGR"/lib/angr_native.dylib
install_name_tool -change libpyvex.dylib "$PYVEX"/lib/libpyvex.dylib "$ANGR"/lib/angr_native.dylib

function zle-line-init zle-keymap-select {
    RPROMPT="[${${KEYMAP/vicmd/NORMAL}/(main|viins)/INSERT}]"
    zle reset-prompt
}
preexec () { print -rn -- $terminfo[el]; }

zle -N zle-line-init
zle -N zle-keymapeselect

export PATH=$HOME/scripts:/usr/local/opt/gnu-indent/libexec/gnubin:/usr/local/Cellar/zlib/1.2.8/lib/pkgconfig:/usr/local/lib/pkgconfig:/opt/X11/lib/pkgconfig:$HOME/.emacs.d/bin:/Library/TeX/texbin/:/usr/local/opt/bison/bin:$HOME/bin:/usr/local/bin:/usr/local/opt/llvm/bin:$PATH
alias vi="nvim"
alias omnisharp-run="/Users/Thomas/.vscode/extensions/ms-vscode.csharp-1.19.0/.omnisharp/1.32.18/run"
export GTAGSLABEL="ctags"
export LPASS_ASKPASS=~/askpass

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
